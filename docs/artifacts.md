# Artifacts

Artifacts are collected on all stages . On the different stages there may be different artifacts like config files on the build stage and some reports on the deployment stage.

Artifacts path:

| path | description | Ansible tag|
|--- | --- |--- | 
| build | some artifacts from the build process (configs/packages)| [build]|
| test | Files or Reports from the Testing stage | [build] |
| state | some artifacts from the deployment process (diffs/reports) | [deploy] |
| docs | some artifacts from the generated documentation | [build,docs]|
